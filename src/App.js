import Layout from "./components/Layout/Layout";
import BurgerBuilder from "./containers/BurgerBuilder/BurgerBuilder";
import "./App.css";
import { Route, Switch, Redirect, withRouter } from "react-router-dom";
import Checkout from "./containers/Checkout/Checkout";
import Orders from "./containers/Orders/Orders";
import Auth from "./containers/Auth/Auth";
import Logout from "./containers/Auth/Logout";
import { connect } from "react-redux";
import * as actions from "./store/actions/index";
import { useEffect } from "react";

function App(props) {
  useEffect(() => {
    props.onTryAutoSignIn();
  }, []);

  return (
    <div>
      <Layout>
        <Switch>
          {props.isAuthenticated && (
            <Route path="/checkout" component={Checkout}></Route>
          )}
          {props.isAuthenticated && (
            <Route path="/orders" component={Orders}></Route>
          )}
          <Route path="/auth" component={Auth}></Route>
          {props.isAuthenticated && (
            <Route path="/logout" component={Logout}></Route>
          )}
          <Route path="/" component={BurgerBuilder}></Route>
          <Redirect to="/" />
        </Switch>
        ;
      </Layout>
    </div>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    onTryAutoSignIn: () => dispatch(actions.authCheckState()),
  };
};

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.auth.token !== null,
  };
};
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
