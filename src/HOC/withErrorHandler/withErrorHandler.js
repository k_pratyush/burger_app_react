import React from "react";
import Modal from "../../components/Modal/Modal";
import useErrorHandler from "../../hooks/httpErrorHandler";

const withErrorHandler = (WrappedComponent, axios) => {
  return (props) => {
    const [error, errorConfirmHandler] = useErrorHandler(axios);
    return (
      <>
        <Modal show={!!error} clicked={errorConfirmHandler}>
          {error ? error.message : null}
        </Modal>
        <WrappedComponent {...props} />{" "}
      </>
    );
  };
};

export default withErrorHandler;
