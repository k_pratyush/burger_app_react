import React, { useState, useEffect, useCallback } from "react";
import BuildControls from "../../components/Burger/BuildControls/BuildControls";
import Burger from "../../components/Burger/Burger";
import Modal from "../../components/Modal/Modal";
import OrderSummary from "../../components/OrderSummary/OrderSummary";
import axios from "../../axios-orders";
import Spinner from "../../components/Spinner/Spinner";
import withErrorHandler from "../../HOC/withErrorHandler/withErrorHandler";
import { useDispatch, useSelector } from "react-redux";
import * as burgerBuilderActions from "../../store/actions/index";

const BurgerBuilder = (props) => {
  const dispatch = useDispatch();

  const onIngredientAdded = (ingName) =>
    dispatch(burgerBuilderActions.addIngredient(ingName));
  const onIngredientRemoved = (ingName) =>
    dispatch(burgerBuilderActions.removeIngredient(ingName));
  const onInitIngredients = useCallback(
    () => dispatch(burgerBuilderActions.initIngredients()),
    [dispatch]
  );
  const onInitPurchase = () => dispatch(burgerBuilderActions.purchaseInit());
  const onSetAuthRedirectPath = (path) =>
    dispatch(burgerBuilderActions.setAuthRedirectPath(path));

  const ings = useSelector((state) => state.burgerBuilder.ingredients);
  const price = useSelector((state) => state.burgerBuilder.price);
  const error = useSelector((state) => state.burgerBuilder.error);
  const isAuthenticated = useSelector((state) => state.auth.token !== null);
  const building = useSelector((state) => state.burgerBuilder.building);

  useEffect(() => {
    onInitIngredients();
    console.log(props);
  }, [onInitIngredients]);

  const [purchasing, setPurchasing] = useState(false);

  const addIngredientHandler = (type) => {
    let newIngredients = { ...ings };
    newIngredients[type] = newIngredients[type] + 1;
    onIngredientAdded(type);
  };

  const removeIngredientHandler = (type) => {
    let newIngredients = { ...ings };
    if (newIngredients[type] > 0) {
      onIngredientRemoved(type);
    }
  };

  const updatePurchasable = () => {
    const sum = Object.keys(ings)
      .map((igKey) => {
        return ings[igKey];
      })
      .reduce((sum, el) => {
        return sum + el;
      }, 0);

    return !!sum;
  };

  const pusrchaseContinueHandler = () => {
    onInitPurchase();
    console.log(props);
    props.history.push("/checkout");
  };

  const disabledInfo = { ...ings };
  for (let key in ings) {
    disabledInfo[key] = ings[key] <= 0;
  }

  let orderSummary = null;

  const purchaseHandler = () => {
    if (isAuthenticated) {
      setPurchasing(true);
    } else {
      onSetAuthRedirectPath("/checkout");
      props.history.push("/auth");
    }
  };

  let burger = error ? <p>something went wrong</p> : <Spinner />;
  if (ings) {
    burger = (
      <>
        <Burger ingredients={ings} />
        <BuildControls
          ingredientAdded={addIngredientHandler}
          ingredientRemoved={removeIngredientHandler}
          disabled={disabledInfo}
          totalPrice={price}
          purchasable={updatePurchasable()}
          ordered={purchaseHandler}
          isAuthenticated={isAuthenticated}
        />
      </>
    );
    orderSummary = (
      <OrderSummary
        ingredients={ings}
        continue={pusrchaseContinueHandler}
        cancel={() => setPurchasing(false)}
        price={price}
      />
    );
  }
  return (
    <>
      <Modal show={purchasing} clicked={() => setPurchasing(false)}>
        {orderSummary}
      </Modal>
      {burger}
    </>
  );
};

export default withErrorHandler(BurgerBuilder, axios);
