import React, { useEffect, useState } from "react";
import CheckoutSummary from "../../components/CheckoutSummary/CheckoutSummary";
import { Route, Redirect } from "react-router-dom";
import ContactData from "../../components/ContactData/ContactData";
import { connect } from "react-redux";

const Checkout = (props) => {
  // const [ingredients, setIngredients] = useState(null);
  // const [price, setPrice] = useState(0);
  useEffect(() => {
    // const query = new URLSearchParams(props.location.search);
    // const ingr = {};
    // let price = 0;
    // console.log(query.entries());
    // for (let param of query.entries()) {
    //   if (param[0] === "price") {
    //     price = param[1];
    //   } else {
    //     ingr[param[0]] = +param[1];
    //   }
    // }
    // setIngredients(ingr);
    // setPrice(price);
    // console.log(ingr);
  }, []);

  const checkoutCancelledHandler = () => {
    props.history.goBack();
  };

  const checkoutContinueHandler = () => {
    props.history.replace("/checkout/contact-data");
  };

  let summary = <Redirect to="/" />;
  if (props.ings) {
    let purchasedRedirect = props.purchased ? <Redirect to="/" /> : null;
    summary = (
      <div>
        {purchasedRedirect}
        <CheckoutSummary
          checkoutContinued={checkoutContinueHandler}
          checkoutCancelled={checkoutCancelledHandler}
          ingredients={props.ings}
        />
        <Route
          path={props.match.path + "/contact-data"}
          component={ContactData}
        />
      </div>
    );
  }

  return summary;
};

const mapStateToProps = (state) => {
  return {
    ings: state.burgerBuilder.ingredients,
    price: state.burgerBuilder.price,
    purchased: state.order.purchased,
  };
};

export default connect(mapStateToProps)(Checkout);
