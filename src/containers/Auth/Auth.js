import React, { useState, useEffect } from "react";
import Button from "../../components/Button/Button";
import Input from "../../components/Input/Input";
import "./Auth.css";
import * as actions from "../../store/actions/index";
import { connect } from "react-redux";
import Spinner from "../../components/Spinner/Spinner";
import { Redirect } from "react-router-dom";

const Auth = (props) => {
  const [loginForm, setLoginForm] = useState({
    email: {
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "Your email",
      },
      value: "",
      validation: {
        required: true,
        isEmail: true,
      },
      touched: false,
      valid: false,
    },
    password: {
      elementType: "input",
      elementConfig: {
        type: "password",
        placeholder: "password",
      },
      value: "",
      validation: {
        required: true,
        minLenth: 6,
      },
      touched: false,
      valid: false,
    },
  });

  useEffect(() => {
    if (!props.buildingBurger && props.authRedirectPath !== "/") {
      props.onSetRedirectPath();
    }
  }, [props]);
  const [isSignup, setIsSignup] = useState(true);

  const formElementArray = [];
  for (let key in loginForm) {
    formElementArray.push({
      id: key,
      config: loginForm[key],
    });
  }

  const checkValidity = (value, rules) => {
    let isValid = true;
    if (rules.required) {
      isValid = value.trim() !== "" && isValid;
    }
    if (rules.maxLength) {
      isValid = value.length >= rules.minLength && isValid;
    }
    if (rules.minLength) {
      isValid = value.length <= rules.minLength && isValid;
    }

    return isValid;
  };

  const inputChangedHandler = (event, inputIdentifier) => {
    let updatedLoginForm = {
      ...loginForm,
    };
    let updatedFormElement = { ...updatedLoginForm[inputIdentifier] };
    updatedFormElement.value = event.target.value;
    updatedFormElement.valid = checkValidity(
      updatedFormElement.value,
      updatedFormElement.validation
    );
    updatedFormElement.touched = true;
    console.log(updatedFormElement);
    updatedLoginForm[inputIdentifier] = updatedFormElement;
    let formIsValid = true;
    for (let inputIdentifier in updatedLoginForm) {
      formIsValid = updatedLoginForm[inputIdentifier].valid && formIsValid;
    }
    // setFormIsValid(formIsValid);
    console.log(formIsValid);

    setLoginForm(updatedLoginForm);
  };

  let form = formElementArray.map((formElement) => (
    <Input
      key={formElement.id}
      id={formElement.id}
      elementType={formElement.config.elementType}
      elementConfig={formElement.config.elementConfig}
      value={formElement.config.value}
      changed={(event) => {
        inputChangedHandler(event, formElement.id);
      }}
      invalid={!formElement.config.valid}
      shouldValidate={formElement.config.validation}
      touched={formElement.config.touched}
    />
  ));

  if (props.loading) {
    form = <Spinner />;
  }

  const submitHandler = (event) => {
    event.preventDefault();
    props.onAuth(loginForm.email.value, loginForm.password.value, isSignup);
  };

  const switchAuthModeHandler = () => {
    setIsSignup((previousState) => !previousState);
  };
  let error = null;
  if (props.error) {
    error = <p>{props.error.message}</p>;
  }
  let authRedirect = null;
  if (props.isAuthenticated) {
    authRedirect = <Redirect to={props.authRedirectPath} />;
  }
  return (
    <div className="Auth">
      {authRedirect}
      {error}
      <form onSubmit={submitHandler}>
        {form}
        <Button btnType="Success">SUBMIT</Button>
      </form>
      <Button btnType="Danger" clicked={switchAuthModeHandler}>
        {isSignup ? "SWITCH TO SIGN IN" : "SWITCH TO SIGN UP"}
      </Button>
    </div>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    onAuth: (email, password, isSignup) =>
      dispatch(actions.auth(email, password, isSignup)),
    onSetRedirectPath: () => dispatch(actions.setAuthRedirectPath("/")),
  };
};

const mapStateToProps = (state) => {
  return {
    loading: state.auth.loading,
    error: state.auth.error,
    isAuthenticated: state.auth.token !== null,
    authRedirectPath: state.auth.authRedirectPath,
    buildingBurger: state.burgerBuilder.building,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Auth);
