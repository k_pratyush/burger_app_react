import axios from "axios";

const instance = axios.create({
  baseURL:
    "https://react-my-burger-backend-30088-default-rtdb.europe-west1.firebasedatabase.app/",
});

export default instance;
