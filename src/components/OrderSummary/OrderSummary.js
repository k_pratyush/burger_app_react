import React from "react";
import Button from "../Button/Button";

function OrderSummary(props) {
  const ingredientSummery = Object.keys(props.ingredients).map((igkey) => {
    return (
      <li key={igkey}>
        <span style={{ textTransform: "capitalize" }}>{igkey}</span>:{" "}
        {props.ingredients[igkey]}
      </li>
    );
  });
  return (
    <>
      <h3>Your Order</h3>
      <p>Ingredients:</p>
      <ul>{ingredientSummery}</ul>
      <p>
        <strong>Total price: {props.price.toFixed(2)}</strong>
      </p>
      <p>Continue to checkout</p>
      <Button btnType="Danger" clicked={props.cancel}>
        CANCEL
      </Button>
      <Button btnType="Success" clicked={props.continue}>
        CONTINUE
      </Button>
    </>
  );
}

export default OrderSummary;
