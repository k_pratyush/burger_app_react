import React, { useState } from "react";
import { connect } from "react-redux";
import Toolbar from "../Navigation/Toolbar/Toolbar";
import SideDrawer from "../SideDrawer/SideDrawer";
import "./Layout.css";

// const classes = useStyle;

const Layout = (props) => {
  const [showSideDrawer, setShowSideDrawer] = useState(false);
  const sideDrawerToggleHandler = () => {
    let newTemp = showSideDrawer;
    setShowSideDrawer(!newTemp);
  };
  return (
    <>
      <Toolbar
        drawerToggleClicked={sideDrawerToggleHandler}
        isAuth={props.isAuth}
      />
      <SideDrawer
        open={showSideDrawer}
        isAuth={props.isAuth}
        clicked={() => {
          setShowSideDrawer(false);
        }}
      />
      <main className="Content">{props.children}</main>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    isAuth: state.auth.token !== null,
  };
};

export default connect(mapStateToProps)(Layout);
