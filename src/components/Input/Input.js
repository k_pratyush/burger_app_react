import React from "react";
import "./Input.css";

const Input = (props) => {
  let inputElement = null;
  let inputClasses = ["InputElement"];
  if (props.invalid && props.shouldValidate && props.touched) {
    inputClasses.push("Invalid");
  }
  let inputClass = inputClasses.join(" ");

  let validationError = null;
  if (props.invalid && props.touched) {
    validationError = <p>Please enter a valid value!</p>;
  }
  switch (props.elementType) {
    case "input":
      inputElement = (
        <input
          key={props.id}
          className={inputClass}
          {...props.elementConfig}
          value={props.value}
          onChange={props.changed}
        />
      );
      break;
    case "textarea":
      inputElement = (
        <textarea
          key={props.id}
          className={inputClass}
          {...props.elementConfig}
          value={props.value}
          onChange={props.changed}
        />
      );
      break;
    case "select":
      inputElement = (
        <select
          key={props.id}
          className={inputClass}
          value={props.value}
          onChange={props.changed}
        >
          {props.elementConfig.options.map((option) => (
            <option key={option.value} value={option.value}>
              {option.displayValue}
            </option>
          ))}
        </select>
      );
      break;
    default:
      inputElement = (
        <input
          key={props.id}
          className={inputClass}
          {...props.elementConfig}
          value={props.value}
        />
      );
  }
  return (
    <div className="Input" key={props.label}>
      <label className="Label">{props.label}</label>
      {inputElement}
      {validationError}
    </div>
  );
};
export default Input;
