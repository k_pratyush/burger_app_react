import React from "react";
import Logo from "../../Logo/Logo";
import DrawerToggle from "../../SideDrawer/DrawerToggle";
import NavigationItems from "../NavigationItems/NavigationItems";
import "./Toolbar.css";

export default function Toolbar(props) {
  return (
    <header className="Toolbar">
      <DrawerToggle clicked={props.drawerToggleClicked} />
      <Logo />
      <nav className="DesktopOnly">
        <NavigationItems isAuth={props.isAuth} />
      </nav>
    </header>
  );
}
