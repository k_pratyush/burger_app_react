import React from "react";
import NavigationItem from "./NavigationItem";
import "./NavigationItems.css";

export default function NavigationItems(props) {
  return (
    <ul className="NavigationItems">
      <NavigationItem link="/">Burger Builder</NavigationItem>
      {props.isAuth && <NavigationItem link="/orders">Orders</NavigationItem>}
      {props.isAuth ? (
        <NavigationItem link="/logout">Logout</NavigationItem>
      ) : (
        <NavigationItem link="/auth">Auth</NavigationItem>
      )}
    </ul>
  );
}
