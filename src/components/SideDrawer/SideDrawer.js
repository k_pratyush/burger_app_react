import React from "react";
import Backdrop from "../Backdrop/Backdrop";
import Logo from "../Logo/Logo";
import NavigationItems from "../Navigation/NavigationItems/NavigationItems";
import "./SideDrawer.css";

function SideDrawer(props) {
  return (
    <>
      <Backdrop show={props.open} clicked={props.clicked} />
      <div
        className={"SideDrawer " + (props.open ? "Open" : "Close")}
        onClick={props.clicked}
      >
        <div style={{ height: "11%", marginBottom: 32 }}>
          <Logo />
        </div>
        <nav>
          <NavigationItems isAuth={props.isAuth} />
        </nav>
      </div>
    </>
  );
}

export default SideDrawer;
