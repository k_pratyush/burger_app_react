import React from "react";
import "./BuildControls.css";
import BuildControl from "./BuildControl/BuildControl";

const controls = [
  { label: "Salad", type: "salad" },
  { label: "Bacon", type: "bacon" },
  { label: "Cheese", type: "cheese" },
  { label: "Meat", type: "meat" },
];

export default function BuildControls({
  ingredientAdded,
  ingredientRemoved,
  disabled,
  totalPrice,
  purchasable,
  ordered,
  isAuthenticated,
}) {
  return (
    <div className="BuildControls">
      <p>
        Current Price : <strong>{totalPrice.toFixed(2)}</strong>{" "}
      </p>
      {controls.map((ctrl) => (
        <BuildControl
          key={ctrl.label}
          label={ctrl.label}
          added={() => ingredientAdded(ctrl.type)}
          removed={() => ingredientRemoved(ctrl.type)}
          disabled={disabled[ctrl.type]}
        />
      ))}
      <button className="OrderButton" disabled={!purchasable} onClick={ordered}>
        {isAuthenticated ? "ORDER NOW" : "SIGN UP TO ORDER"}
      </button>
    </div>
  );
}
