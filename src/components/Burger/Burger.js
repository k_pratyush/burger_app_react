import React, { useEffect } from "react";
import "./Burger.css";
import BurgerIngredient from "./BurgerIngredients/BurgerIngredient";
function Burger({ ingredients }) {
  console.log(ingredients);
  // let transformIngredients = null;
  // useEffect(() => {
  // if (ingredients) {
  const transformIngredients = Object.keys(ingredients)
    .map((igKey) => {
      return [...Array(ingredients[igKey])].map((_, i) => {
        return <BurgerIngredient key={igKey + 1} type={igKey} />;
      });
    })
    .reduce((arr, el) => {
      return arr.concat(el);
    }, []);
  // }
  // }, []);

  console.log(transformIngredients);
  return (
    <div className="Burger">
      <BurgerIngredient type="bread-top" />
      {transformIngredients}
      <BurgerIngredient type="bread-bottom" />
    </div>
  );
}

export default Burger;
