import React from "react";
import Burger from "../Burger/Burger";
import Button from "../Button/Button";
import "./CheckoutSummary.css";

const CheckoutSummary = (props) => {
  return (
    <div className="CheckoutSummary">
      <h1>We hope it taste well</h1>
      <div style={{ width: "100%", margin: "auto" }}>
        <Burger ingredients={props.ingredients} />
        <Button clicked={props.checkoutCancelled} btnType="Danger">
          CANCEL
        </Button>
        <Button clicked={props.checkoutContinued} btnType="Success">
          CONTINUE
        </Button>
      </div>
    </div>
  );
};
export default CheckoutSummary;
