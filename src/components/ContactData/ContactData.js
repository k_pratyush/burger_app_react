import React, { useState } from "react";
import Button from "../Button/Button";
import "./ContactData.css";
import axios from "../../axios-orders";
import Spinner from "../Spinner/Spinner";
import Input from "../Input/Input";
import { connect } from "react-redux";
import withErrorHandler from "./../../HOC/withErrorHandler/withErrorHandler";
import * as actions from "../../store/actions";

const ContactData = (props) => {
  const [orderForm, setOrderForm] = useState({
    name: {
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "Your name",
      },
      value: "",
      validation: {
        required: true,
      },
      touched: false,
      valid: false,
    },
    street: {
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "street",
      },
      value: "",
      validation: {
        required: true,
      },
      touched: false,
      valid: false,
    },
    zipCode: {
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "zipcode",
      },
      value: "",
      validation: {
        required: true,
        minLength: 5,
        maxLength: 5,
      },
      touched: false,
      valid: false,
    },
    country: {
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "country",
      },
      value: "",
      validation: {
        required: true,
      },
      touched: false,
      valid: false,
    },

    email: {
      elementType: "input",
      elementConfig: {
        type: "email",
        placeholder: "email",
      },
      value: "",
      validation: {
        required: true,
      },
      touched: false,
      valid: false,
    },

    deliveryMethod: {
      elementType: "select",
      elementConfig: {
        options: [
          { value: "fastest", displayValue: "Fastest" },
          { value: "cheapest", displayValue: "Cheapest" },
        ],
      },
      valid: true,
      validation: {},
      value: "fastest",
    },
  });
  const [formIsValid, setFormIsValid] = useState(false);
  // const [loading, setLoading] = useState(false);
  const orderHandler = (event) => {
    event.preventDefault();
    const formData = {};
    for (let formElementIdentifier in orderForm) {
      formData[formElementIdentifier] = orderForm[formElementIdentifier].value;
    }

    const order = {
      ingredients: props.ings,
      price: props.price,
      orderData: formData,
      userId: props.userId,
    };

    props.onOrderBurger(order, props.token);
  };

  const checkValidity = (value, rules) => {
    let isValid = true;
    if (rules.required) {
      isValid = value.trim() !== "" && isValid;
    }
    if (rules.maxLength) {
      isValid = value.length >= rules.minLength && isValid;
    }
    if (rules.minLength) {
      isValid = value.length <= rules.minLength && isValid;
    }

    return isValid;
  };

  const formElementArray = [];
  for (let key in orderForm) {
    formElementArray.push({
      id: key,
      config: orderForm[key],
    });
  }
  const inputChangedHandler = (event, inputIdentifier) => {
    let updatedOrderForm = {
      ...orderForm,
    };
    let updatedFormElement = { ...updatedOrderForm[inputIdentifier] };
    updatedFormElement.value = event.target.value;
    updatedFormElement.valid = checkValidity(
      updatedFormElement.value,
      updatedFormElement.validation
    );
    updatedFormElement.touched = true;
    console.log(updatedFormElement);
    updatedOrderForm[inputIdentifier] = updatedFormElement;
    let formIsValid = true;
    for (let inputIdentifier in updatedOrderForm) {
      formIsValid = updatedOrderForm[inputIdentifier].valid && formIsValid;
    }
    setFormIsValid(formIsValid);
    console.log(formIsValid);

    setOrderForm(updatedOrderForm);
  };
  let form = (
    <form onSubmit={orderHandler}>
      {formElementArray.map((formElement) => (
        <Input
          key={formElement.id}
          id={formElement.id}
          elementType={formElement.config.elementType}
          elementConfig={formElement.config.elementConfig}
          value={formElement.config.value}
          changed={(event) => {
            inputChangedHandler(event, formElement.id);
          }}
          invalid={!formElement.config.valid}
          shouldValidate={formElement.config.validation}
          touched={formElement.config.touched}
        />
      ))}
      <Button btnType="Success" disabled={!formIsValid}>
        ORDER
      </Button>
    </form>
  );

  if (props.loading) {
    form = <Spinner />;
  }
  return (
    <div className="ContactData">
      <h4>Enter your contact data</h4>
      {form}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    ings: state.burgerBuilder.ingredients,
    price: state.burgerBuilder.price,
    loading: state.order.loading,
    token: state.auth.token,
    userId: state.auth.userId,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onOrderBurger: (orderData, token) =>
      dispatch(actions.purchaseBurger(orderData, token)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withErrorHandler(ContactData, axios));
