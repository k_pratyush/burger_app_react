import React from "react";
import "./Button.css";

export default function Button(props) {
  console.log(props);
  return (
    <button
      className={"Button " + props.btnType}
      disabled={props.disabled}
      onClick={props.clicked}
    >
      {props.children}
    </button>
  );
}
