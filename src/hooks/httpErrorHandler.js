import { useEffect, useState } from "react";

export default (axios) => {
  const [error, setError] = useState(null);

  let reqInterceptor = axios.interceptors.request.use((request) => {
    setError(null);
    return request;
  });
  let respInterceptor = axios.interceptors.response.use(
    (res) => res,
    (error) => {
      setError(error);
    }
  );

  useEffect(() => {
    return () => {
      axios.interceptors.request.eject(reqInterceptor);
      axios.interceptors.response.eject(respInterceptor);
    };
  }, []);
  const errorConfirmHandler = () => {
    setError(null);
  };

  return [error, errorConfirmHandler];
};
